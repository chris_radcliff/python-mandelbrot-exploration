import math
import png

class clpx(): #class for complex numbers
    
    def __init__(self,a,b):
        self.a=a #real part
        self.b=b #imaginary part
        self.divergence=False
        self.divergence_count = 0
        self.colorR=0
        self.colorG=0
        self.colorB=0
        
    def __add__(self,other):
        real = self.a + other.a
        imaginary = self.b + other.b

        return clpx(real, imaginary) #YOU CODE
    
    def __eq__(self,other):
        if self.a==other.a and self.b==other.b:
            return True
        return False
    
    def __mul__(self, other):
        # Since (a + bi) * (c + di)
        # equals (a * c) + (a * di) + (bi * c) + (bi * di)
        # equals (ac) + (ad + bc)i + -(bd),
        # then the result is (ac - bd) + (ad + bc)i
        real = self.a * other.a - self.b * other.b
        imaginary = self.a * other.b + self.b * other.a

        return clpx(real, imaginary)
    
    def __str__(self):
        round_depth=5
        if self.b>=0:
            sgn=" + "
        else:
            sgn=" "
        return str(round(self.a,round_depth)) + sgn + str(round(self.b,round_depth)) + "i"
    
    def __repr__(self): #What does "repr" do?
        round_depth=3
        if self.b>=0:
            sgn=" + "
        else:
            sgn=" "
        return str(round(self.a,round_depth)) + sgn + str(round(self.b,round_depth)) + "i"
    
    def __abs__(self):
        # Absolute value is the distance from zero
        return self.distance(clpx(0, 0)) #YOU CODE     
        
    def distance(self,other):
        real_distance = self.a - other.a
        imaginary_distance = self.b - other.b

        distance = math.sqrt( real_distance ** 2 + imaginary_distance ** 2 )

        return distance #YOU CODE
    
    def check_divergence(self):
        # Use this value as the constant c
        c = clpx(self.a, self.b)

        # z1 = z0^2 + c, check if the zs exceed the range
        current_z = clpx(0, 0)
        for i in range(255):
            current_z = (current_z * current_z) + c

            if current_z.__abs__() > 12.0:
                self.divergence = True
                self.divergence_count = i
                break

IMAGE_SIZE = 1000
GRAPH_SIZE = 2.0 # Extent of graph, from -SIZE to SIZE

width = IMAGE_SIZE
height = IMAGE_SIZE
img = []

for x in range(IMAGE_SIZE):
    row = ()

    for y in range(IMAGE_SIZE):
        # Since the image is from, say, 0 to 1000 left to right
        # and the graph is from, say, -2.0 to 2.0 bottom to top
        # determine the graph coordinates from the image coordinates
        graph_y = float(x - (IMAGE_SIZE / 2)) * GRAPH_SIZE * 2.0 / float(IMAGE_SIZE)
        graph_x = float(y - (IMAGE_SIZE / 2)) * GRAPH_SIZE * 2.0 / float(IMAGE_SIZE)

        # print(f"{x, y} becomes {graph_x, graph_y}")

        z = clpx(graph_x, graph_y)
        z.check_divergence()
        color_R = 0
        color_G = 0
        color_B = 0

        if z.divergence == True:
            # Set a color based on how divergent
            color_R = min(z.divergence_count * 2, 255)
            color_G = min(z.divergence_count * 3, 255)
            color_B = z.divergence_count

        row = row + (color_R, color_G, color_B)
    
    img.append(row)

with open('mandelbrot.png', 'wb') as f:
    w = png.Writer(width, height, greyscale=False)
    w.write(f, img)













