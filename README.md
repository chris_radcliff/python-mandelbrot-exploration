# Python Mandelbrot exploration

A quick little bit of fun, inspired by @futurebird@sauropods.win on Mastodon: https://sauropods.win/@futurebird

## Dependencies

As written, this requires a `png` library to generate PNG image files. It should be installed using `pip` at the command line like so:

```
pip install pypng
```

## Generating a Mandelbrot set

The Mandelbrot set is a fractal. As such it really shines at high resolutions, but it also takes considerable time and math to generate.

This application isn't optimized, so it may take a few hours (yes, hours) to create a single 10,000-pixel-wide image. If you'd like a quicker indication of the output, I suggest changing `IMAGE_SIZE` to something like 500 pixels to start.

To run the application:

```
python src/generate_mandelbrot_png.py
```

The output of the application will show up as `mandelbrot.png` in the root directory once the application finishes.

## Authors and acknowledgment

This is heavily derived from code by @futurebird@sauropods.win, who uses that code for teaching purposes. I offer this for educational purposes only, with no claim to copyright.

